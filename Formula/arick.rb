class Arick < Formula
  desc "這是一個測試用的 brew formula"
  homepage "https://gitlab.com/-/ide/project/cwchiu/brewformulaetest"
  url "https://github.com/libxls/libxls/releases/download/v1.5.0/libxls-1.5.0.tar.gz"
  sha256 "c53a55187aeb0545e6fb5cfc89ad9521af80b1afc7eff407ac94c1bd59a64202"
  head "https://gitlab.com/cwchiu/brewformulaetest.git", :branch => "master"

  def install
    print "#1" 
    print "#2"
    print "#3"
    system "echo 123 > /tmp/123"
  end

  test do
    output = pipe_output("cat /tmp/123 2>/dev/null")
    assert_equal "123", output.lines.last.chomp
  end
end
